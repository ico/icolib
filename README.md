# ICOlib


This project is the core library of the ICO tool suite.



## Developement
This project relies on several libraries. You must add them to your classpath in to order to compile the project:
- [de.ovgu.featureide.lib.fm-v3.9.0.jar](https://github.com/FeatureIDE/FeatureIDE/releases/download/v3.9.0/de.ovgu.featureide.lib.fm-v3.9.0.jar)
- [org.sat4j.core.jar](https://github.com/FeatureIDE/FeatureIDE/raw/9666caac13b4494bcbe773aac5c09569c24a227f/plugins/de.ovgu.featureide.fm.core/lib/org.sat4j.core.jar)

Complete installation on Eclipse:
- clone the project with ```git@gitlab.inria.fr:ico/icolib.git```
- Open the project with Eclipse: File -> "Open Project from file system" -> with the "directory" button, browse to the location where you cloned the project, and select the icolib folder. Select "open" to select this folder. Make sure that the "icolib" project is selected in the project import window and select "finish".
- Add the dependancies to your classpath: Right click on the icolib project in Eclipse, "Build Path" -> "Configure Build Path". In the project properties window, select "classpath" and then "Add external JARs". Browse to the location where you downloaded the dependancies, select both of them and select "open". In the project properties window, select "Apply and close".

The project is now ready to compile. To export the library:
- Right click on icolib in Eclipse -> "Export"
- Select JAR file -> "Next"
- Make sure the icolib project is selected, and the only selected project.
- Optionally, selecting "Export java source files and ressource" can help the developement of tools using icolib.
- Configure the export destination to where you want the jar file.
- Select "Finish"

## Usage

The icolib library supports performance file named "performance1.csv" for features and "performance2.csv".

The performances must be listed as "p,f" with p the performance (as a number to maximise) and p the name of the feature, or the name of both features of a pair separated with "+". In order to minimize performances, turn the performance negative by adding a "-" symbol in from of the performance value.

Example of performance1.csv:
```
10,feat1
22,feat2
3,feat3
```
Example of performance2.csv:
```
5,feat1+feat2
12,feat1+feat3
30,feat2+feat3
```


The icolib library expose a set of objects and methods to optimize configurations.

FileHandling.loadProjectFiles(Path projectLocation, Path configPath) takes the path to a project, and the path to a configuration within that project, and returns a ConfigImprovement object.

The ConfigImprovement object offers the following methods:
| Method                                                                   | Parameters            | Returns                      | Description                                                                                                                     |
|--------------------------------------------------------------------------|-----------------------|------------------------------|---------------------------------------------------------------------------------------------------------------------------------|
| getSuggestion                                                            | Optimization, boolean | List\<Suggestion\>             | Get the list of suggetions of a specific optimization, with or without logging them                                             |
| applyAllSuggestions                                                      | Optimization          | List\<Suggestion\>             | Apply all the iterative best suggetions of a specific optimization                                                              |
| applySuggestion                                                          | Suggestion            | void                         | Apply the given suggestion                                                                                      |
| getPerformanceItems                                                      | Optimization          | List\<ConfigPerformanceItem\>  | Get the list of feature or interaction ConfigPerformanceItem (make sure that Optimization.getPerformance().isEmpty() returns false)               |
| attachFeatureModel                                                       | Path                  | void                         | Attach the feature model at the given path to the configuration                                                                 |
| save                                                                     |                       | boolean                      | Override the configuration file with current changes                                                                            |
| save                                                                     | Path                  | boolean                      | Save the configuration and current changes to the given path                                                                    |
| getFeatureOptimization<br /> getInteractionOptimization                  |                       | Optimization                 | Get the feature-wise or interaction-wise Optimization object                                                                    |
| getInclude<br />getExclude                                               |                       | Collection\<SelectableFeature> | Get the list of included or exclude features                                                                                    |
| getIncludeName<br />getExcludeName                                       |                       | Collection\<String\>           | Get the list of included or exclude features' names                                                                             |
| addExclusion<br />addInclusion<br />removeExclusion<br />removeExclusion | String                |                              | Add or remove the feature with the given name from the inclusion or exclusion list                                              |
| getAllFeatureNames                                                       |                       | Collection\<String\>           | Get a list of all features' names                                                                                               |
| getFeatureFromName                                                       | String                | Feature                      | Get the feature with the given name                                                                                             |
| getPath                                                                  |                       | Path                         | Get the path of the configuration                                                                                               |


## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
If you want to contribute to this project please create a merge request.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.


