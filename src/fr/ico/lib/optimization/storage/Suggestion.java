package fr.ico.lib.optimization.storage;

import java.util.Optional;

import de.ovgu.featureide.fm.core.configuration.SelectableFeature;

/**
 * 
 * @author Edouard GUEGAIN
 *
 */
public class Suggestion {
	private Optional<SelectableFeature> toRemove;
	private Optional<SelectableFeature> toAdd;
	
	private Double initialPerf;
	private Double newPerf;
	
	public Suggestion(SelectableFeature toRemove, SelectableFeature toAdd) {
		super();
		this.toRemove = Optional.ofNullable(toRemove);
		this.toAdd = Optional.ofNullable(toAdd);
	}
	
	public Suggestion computeDelta(double initialPerf, double newPerf) {
		this.initialPerf = initialPerf;
		this.newPerf = newPerf;
		return this;
	}
	
	public Double getPercentage() {
		return Math.abs((this.getDelta() / this.initialPerf) * 100);
	}

	public Double getDelta() {
		return newPerf - initialPerf;
	}

	public Optional<SelectableFeature> getToRemove() {
		return toRemove;
	}

	public Optional<SelectableFeature> getToAdd() {
		return toAdd;
	}

	public String getToRemoveName() {
		return toRemove.map(SelectableFeature::getName).orElse("-");
	}

	public String getToAddName() {
		return toAdd.map(SelectableFeature::getName).orElse("-");
	}

	public String toString() {
		return "-" + toRemove.map(SelectableFeature::getName).orElse("N/A") 
				+ " +" + toAdd.map(SelectableFeature::getName).orElse("N/A") 
				+ ", " + this.initialPerf + " -> " + this.newPerf + ", " + this.getDelta();
	}

	
}
