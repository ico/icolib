package fr.ico.lib.optimization.storage;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Edouard GUEGAIN
 *
 */
public class Performance {
	
	private Map<String, Integer> performances = new HashMap<String, Integer>();
	
	public Performance(Map<String, Integer> performances) {
		this.performances = performances;
	}
	
	public Integer ofFeature(String feature) {
		return performances.get(feature);
	}
	
	public Integer ofInteraction(String featureA, String featureB) {
		Integer left = performances.get(featureA + "+" + featureB);
		Integer right = performances.get(featureB + "+" + featureA);
		return left != null ? left : (right != null ? right : null);
	}
	
	public boolean isEmpty() {
		return this.performances.isEmpty();
	}
	
}