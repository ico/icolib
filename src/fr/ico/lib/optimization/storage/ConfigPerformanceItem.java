package fr.ico.lib.optimization.storage;

public class ConfigPerformanceItem {

	private String itemName;
	private Double performance;
	
	public ConfigPerformanceItem(double performance, String itemName) {
		this.itemName = itemName;
		this.performance = performance;
	}

	public String getLabel() {
		return itemName;
	}
	
	public Double getPerformance() {
		return performance;
	}
	
	public String toString() {
		return this.itemName + " " + this.performance;
	}
}
