package fr.ico.lib.optimization;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import de.ovgu.featureide.fm.core.configuration.Configuration;
import de.ovgu.featureide.fm.core.configuration.ConfigurationAnalyzer;
import de.ovgu.featureide.fm.core.configuration.SelectableFeature;
import de.ovgu.featureide.fm.core.configuration.Selection;
import de.ovgu.featureide.fm.core.init.FMCoreLibrary;
import de.ovgu.featureide.fm.core.init.LibraryManager;
import de.ovgu.featureide.fm.core.io.manager.ConfigurationManager;
import de.ovgu.featureide.fm.core.io.manager.FeatureModelManager;
import fr.ico.lib.optimization.storage.ConfigPerformanceItem;
import fr.ico.lib.optimization.storage.Performance;
import fr.ico.lib.optimization.storage.Suggestion;
import fr.ico.lib.optimization.utils.Logger;

/**
 * 
 * @author Edouard GUEGAIN
 *
 */
public class ConfigImprovement {
	
	private ConfigurationManager configManager;
	private Performance featurePerfs;
	private Performance interactionPerfs;
	private Configuration config;

	private List<SelectableFeature> include = new ArrayList<>();
	private List<SelectableFeature> exclude = new ArrayList<>();
	
	public ConfigImprovement(Path configurationPath, Performance featurePerfs, Performance interactionPerfs) {
		LibraryManager.registerLibrary(FMCoreLibrary.getInstance());
		
		if (!ConfigurationManager.isFileSupported(configurationPath)) {
			throw new IllegalArgumentException("Configuration file format not supported: " + configurationPath.getFileName());
		}

		this.configManager = ConfigurationManager.getInstance(configurationPath);

		this.featurePerfs = featurePerfs;
		this.interactionPerfs = interactionPerfs;
		this.config = this.configManager.getObject();
		this.configManager.update();
	}
	
	public List<Suggestion> getSuggestions(Optimization optimization, boolean logSuggestions) {
		List<Suggestion> suggestions = new ArrayList<>();

		long startFeature = System.currentTimeMillis();
		
		try {
			suggestions.addAll(optimization.findAdditionalFeatures(config));
		} catch (Exception e) {
			Logger.err(e.getMessage());
			e.printStackTrace();
		}
		
		if (new ConfigurationAnalyzer(config.getFeatureModelFormula(), config).isValid()) {
			suggestions.addAll(optimization.findRemovableFeatures(config));
			suggestions.addAll(optimization.findSubstitutions(config));
		}
		suggestions.sort(Comparator.comparing(Suggestion::getDelta).reversed());
		long endFeature = System.currentTimeMillis();
		
		if(logSuggestions) {
			Logger.log("Suggestion - " + optimization.toString() +  ": (" + suggestions.size() + " suggestions in " + (endFeature - startFeature) + "ms)");
			Logger.log(IntStream.range(0, suggestions.size()).mapToObj(i -> "[" + i + "]" + suggestions.get(i)).collect(Collectors.joining("\n")));
		}
		
		return suggestions;
	}
	
	public List<Suggestion> applyAllSuggestions(Optimization optimization) {
		List<Suggestion> appliedSuggestions = new ArrayList<>();
		try {
			appliedSuggestions = applyAllSuggestions(optimization, getSuggestions(optimization, false), new ArrayList<>());
		} catch (Exception e) {
			Logger.err(e.getMessage());
			e.printStackTrace();
		}
		return appliedSuggestions;
	}
	
	private List<Suggestion> applyAllSuggestions(Optimization optimization, List<Suggestion> suggestion, List<Suggestion> applied) throws Exception {
		suggestion.stream().findFirst().ifPresent(s -> {this.applySuggestion(s); applied.add(s);});
		return suggestion.isEmpty() ? applied : applyAllSuggestions(optimization, getSuggestions(optimization, false), applied);
	}
	
	public void applySuggestion(Suggestion suggestion) {
		applySuggestion(suggestion, this.config);
	}

	public static Configuration applySuggestion(Suggestion suggestion, Configuration config) {
		suggestion.getToRemove().ifPresent(s -> changeFeatureSelection(config, s.getName(), Selection.UNSELECTED, (p, c) -> unselectParents(p, c)));
		suggestion.getToAdd().ifPresent(s -> changeFeatureSelection(config, s.getName(), Selection.SELECTED, (p, c) -> selectParents(p, c)));
		return config;
	}
	
	private interface ParentPropagator {
	    void updateParentSelection(SelectableFeature parent, Configuration config);
	}

	private static void selectParents(SelectableFeature parent, Configuration config) {
		if (parent == null) return;
		changeFeatureSelection(config, parent.getName(), Selection.SELECTED, (p,  c) -> selectParents(p, c));
	}

	private static void unselectParents(SelectableFeature parent, Configuration config) {
		if (parent == null) return;
		
		long count = config.getFeatures().stream()
				.filter(f -> f != null)
				.filter(f -> f.getParent() != null)
				.filter(f -> f.getSelection().equals(Selection.SELECTED))
				.filter(f -> ((SelectableFeature)f.getParent()).getName().equals(parent.getName()))
				.count();

		if (count != 0) return;
		changeFeatureSelection(config, parent.getName(), Selection.UNDEFINED, (p, c) -> unselectParents(p, c));
	}
	
	private static void changeFeatureSelection(Configuration config, String featureName, Selection selection, ParentPropagator parentPropagator) {
		SelectableFeature feature = config.getSelectableFeature(featureName);
		config.setAutomatic(feature, Selection.UNDEFINED);
		config.setManual(feature, selection);
		parentPropagator.updateParentSelection((SelectableFeature) feature.getParent(), config);
	}
	
	public void attachFeatureModel(Path path) {
		FeatureModelManager fmManager = FeatureModelManager.getInstance(path);
		this.configManager.linkFeatureModel(fmManager);
	}
	
	public boolean save() {
		return save(configManager.getPath());
	}
	
	public boolean save(Path path) {
		return ConfigurationManager.save(configManager.getObject(), path, configManager.getFormat());
	}
	
	public Suggestion createSuggestion(Optimization optimization, String toRemove, String toAdd) {
		SelectableFeature featureToAdd = toAdd == null ? null : getFeatureFromName(toAdd);
		SelectableFeature featureToRemove = toRemove == null ? null : getFeatureFromName(toRemove);
		return optimization.createSuggestion(config, featureToRemove, featureToAdd);
	}

	public void addExclusion(String featureName) {
		this.exclude.add(this.getFeatureFromName(featureName));
	}
	
	public void removeExclusion(String featureName) {
		this.exclude.remove(this.getFeatureFromName(featureName));
	}

	public void addInclusion(String featureName) {
		this.include.add(this.getFeatureFromName(featureName));
	}
	
	public void removeInclusion(String featureName) {
		this.include.remove(this.getFeatureFromName(featureName));
	}
	
	public boolean isSuggestionValid(Suggestion s) {
		Configuration newConfig = applySuggestion(s, config.clone());
		return new ConfigurationAnalyzer(newConfig.getFeatureModelFormula(), newConfig).isValid();
	}
	
	public boolean isConfigValid() {
		return new ConfigurationAnalyzer(config.getFeatureModelFormula(), config).isValid();
	}
	
	public boolean canSuggestionBeValid(Suggestion s) {
		Configuration newConfig = applySuggestion(s, config.clone());
		return new ConfigurationAnalyzer(newConfig.getFeatureModelFormula(), newConfig).canBeValid();
	}
	
	public boolean canConfigValid() {
		return new ConfigurationAnalyzer(config.getFeatureModelFormula(), config).canBeValid();
	}

	public List<ConfigPerformanceItem> getPerformanceItems(Optimization optimization) {
		return optimization.generateConfigPerformanceItems(config);
	}

	public Optimization getFeatureOptimization() {
		return new FirstLevelOptimization(this, featurePerfs);
	}
	
	public Optimization getInteractionOptimization() {
		return new SecondLevelOptimization(this, interactionPerfs);
	}

	public Collection<String> getIncludeNames() {
		return include.stream().map(SelectableFeature::getName).collect(Collectors.toList());
	}

	public Collection<String> getExcludeNames() {
		return exclude.stream().map(SelectableFeature::getName).collect(Collectors.toList());
	}

	public Collection<SelectableFeature> getInclude() {
		return include;
	}

	public Collection<SelectableFeature> getExclude() {
		return exclude;
	}
	
	public Collection<String> getSelectedFeatureNames() {
		return config.getSelectedFeatureNames();
	}
	
	public Collection<String> getSelectableFeatureNames() {
		ArrayList<String> names = new ArrayList<>();
		names.addAll(config.getUndefinedFeatureNames());
		names.addAll(config.getUnselectedFeatureNames());
		return names;
	}
	
	public Collection<String> getAllFeatureNames() {
		return this.configManager.getObject().getFeatures().stream().map(SelectableFeature::getName).collect(Collectors.toList());
	}
	
	private SelectableFeature getFeatureFromName(String name) {
		return this.configManager.getObject().getSelectableFeature(name);
	}
	
	public Path getPath() {
		return configManager.getPath();
	}
	
}
