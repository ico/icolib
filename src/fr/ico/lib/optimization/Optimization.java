package fr.ico.lib.optimization;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.ovgu.featureide.fm.core.configuration.Configuration;
import de.ovgu.featureide.fm.core.configuration.ConfigurationAnalyzer;
import de.ovgu.featureide.fm.core.configuration.SelectableFeature;
import fr.ico.lib.optimization.storage.ConfigPerformanceItem;
import fr.ico.lib.optimization.storage.Performance;
import fr.ico.lib.optimization.storage.Suggestion;

/**
 * 
 * @author Edouard GUEGAIN
 *
 */
public abstract class Optimization {
	
	private ConfigImprovement configImprovement;
	private Performance performance;
	private int max = Integer.MAX_VALUE;
	
	public Optimization(ConfigImprovement configImprovement, Performance performance) {
		this.configImprovement = configImprovement;
		this.performance = performance;
	}

	public abstract List<ConfigPerformanceItem> generateConfigPerformanceItems(Configuration config);
	public abstract String toString();
	
	public Suggestion createSuggestion(Configuration config, SelectableFeature toRemove, SelectableFeature toAdd) {
		Suggestion s = new Suggestion(toRemove, toAdd);
		double initialPerf = performance(config);
		double finalPerf = performance(createCandidateConfig(s, config));
		s.computeDelta(initialPerf, finalPerf);
		return s;
	}
	
	public double performance(Configuration config) {
		return generateConfigPerformanceItems(config).stream()
				.map(cpi -> cpi.getPerformance())
				.collect(Collectors.summarizingInt(Double::intValue))
				.getAverage();
	}

	public List<Suggestion> findAdditionalFeatures(Configuration config) throws Exception {
		ConfigurationAnalyzer analyzer = new ConfigurationAnalyzer(config.getFeatureModelFormula(), config);
		double initialPerf = performance(config);
		
		List<List<String>> solution = analyzer.getSolutions(max);
		
		return solution.parallelStream()
				.flatMap(List::stream)
				.filter(s -> !config.getSelectedFeatureNames().contains(s))
				.distinct()
				.map(s -> config.getSelectableFeature(s))
				.filter(f -> !this.getExclude().contains(f))
				.map(f -> new Suggestion(null, f))
				.map(s -> s.computeDelta(initialPerf, performance(createCandidateConfig(s, config))))
				.filter(s -> s.getDelta() > 0)
				.collect(Collectors.toList());
	}
	
	public List<Suggestion> findSubstitutions(Configuration config) {
		double initialPerf = performance(config);
		return config.getSelectedFeatures().parallelStream()
				.filter(f -> f != null)
				.map(config::getSelectableFeature)
				.filter(f -> !this.getInclude().contains(f))
				.map(f -> new Suggestion(f, null))
				.filter(s -> !isConfigurationValide(createCandidateConfig(s, config)))
				.map(s -> completeSuggestion(s, config))
				.flatMap(List::stream)
				.filter(s -> isConfigurationValide(createCandidateConfig(s, config)))
				.filter(s -> s.getToAdd() != s.getToRemove())
				.map(s -> s.computeDelta(initialPerf, performance(createCandidateConfig(s, config))))
				.filter(s -> s.getDelta() > 0)
				.collect(Collectors.toList());
	}
	
	public List<Suggestion> completeSuggestion(Suggestion s1, Configuration config) {
		return Stream.concat(config.getUndefinedSelectedFeatures().stream(), config.getUnSelectedFeatures().stream())
				.filter(f -> f != null)
				.map(config::getSelectableFeature)
				.filter(f -> !this.getExclude().contains(f))
				.map(f -> new Suggestion(s1.getToRemove().get(), f))
				.collect(Collectors.toList());
	}
	
	public List<Suggestion> findRemovableFeatures(Configuration config) {
		double initialPerf = performance(config);
		return config.getSelectedFeatures().stream()
				.filter(f -> f != null)
				.map(config::getSelectableFeature)
				.filter(f -> !this.getInclude().contains(f))
				.map(f -> new Suggestion(f, null))
				.filter(s -> isConfigurationValide(createCandidateConfig(s, config)))
				.map(s -> s.computeDelta(initialPerf, performance(createCandidateConfig(s, config))))
				.filter(s -> s.getDelta() > 0)
				.collect(Collectors.toList());
	}

	public boolean isConfigurationValide(Configuration config) {
		return config == null ? false : new ConfigurationAnalyzer(config.getFeatureModelFormula(), config).isValid();
	}
	
	public Configuration createCandidateConfig(Suggestion suggestion, Configuration config) {
		return ConfigImprovement.applySuggestion(suggestion, config.clone());
	}
	
	public Performance getPerformance() {
		return this.performance;
	}
	
	public Collection<SelectableFeature> getInclude() {
		return configImprovement.getInclude();
	}
	
	public Collection<SelectableFeature> getExclude() {
		return configImprovement.getExclude();
	}
	

}
