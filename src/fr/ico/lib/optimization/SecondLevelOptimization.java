package fr.ico.lib.optimization;

import java.util.List;
import java.util.stream.Collectors;

import de.ovgu.featureide.fm.core.base.IFeature;
import de.ovgu.featureide.fm.core.configuration.Configuration;
import fr.ico.lib.optimization.storage.ConfigPerformanceItem;
import fr.ico.lib.optimization.storage.Performance;

public class SecondLevelOptimization extends Optimization {
	
	public class Pair {
		String left;
		String right;
		
		public Pair(String left, String right) {
			this.left = left;
			this.right = right;
		}
		
	}

	public SecondLevelOptimization(ConfigImprovement configImprovement, Performance performance) {
		super(configImprovement, performance);
	}
	
	@Override
	public List<ConfigPerformanceItem> generateConfigPerformanceItems(Configuration config) {
		List<String> features = config.getSelectedFeatures().stream()
				.filter(f -> f != null)
				.map(IFeature::getName)
				.collect(Collectors.toList());
		
		List<Pair> combinations = features.stream()
				.flatMap(str1 -> features.stream().map(str2 -> new Pair(str1, str2)))
				.filter(p -> !p.left.equals(p.right))
				.collect(Collectors.toList());
		
		return features.parallelStream()
				.map(f -> interactionScore(f, combinations))
				.flatMap(List::stream)
				.collect(Collectors.toList());
	}

	public List<ConfigPerformanceItem> interactionScore(String feature, List<Pair> pairs) {
		return pairs.stream()
				.filter(c -> c.left.equals(feature))
				.filter(c -> this.getPerformance().ofInteraction(c.left, c.right) != null)
				.map(c -> new ConfigPerformanceItem(this.getPerformance().ofInteraction(c.left, c.right), (c.left + "+" + c.right)))
				.collect(Collectors.toList());
 	}

	@Override
	public String toString() {
		return "Interactions";
	}

}
