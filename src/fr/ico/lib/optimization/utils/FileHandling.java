package fr.ico.lib.optimization.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import fr.ico.lib.optimization.ConfigImprovement;
import fr.ico.lib.optimization.storage.Performance;

public class FileHandling {

	public static Performance readPerfFile(File file) throws IOException {
		if (!file.exists()) return null;
		
		Map<String, Integer> performances = new HashMap<String, Integer>();
		String line = "";
		BufferedReader br = new BufferedReader(new FileReader(file));
		while ((line = br.readLine()) != null) {
			try {
				String[] feature = line.split(",");
				performances.put(feature[1], Integer.parseInt(feature[0]));
			} catch (NumberFormatException e){
				Logger.err("Erreur in " + file.getName() + " with line \"" + line + "\"");
			}
		}
		br.close();
		
		Logger.log(file.getName() + " - data points found: " + performances.size());
		return new Performance(performances);
	}

	public static ConfigImprovement loadProjectFiles(Path projectLocation, Path configPath) throws IOException {
		Logger.log("Loading file " + configPath);
		
		Performance featurePerf = readPerfFile(new File(projectLocation + "/performance1.csv"));
		Performance interactionPerf = readPerfFile(new File(projectLocation + "/performance2.csv"));

		return new ConfigImprovement(configPath, featurePerf, interactionPerf);
	}

}
