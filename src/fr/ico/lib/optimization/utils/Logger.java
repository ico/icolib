package fr.ico.lib.optimization.utils;

import java.util.ArrayList;
import java.util.List;

public class Logger {
	
	public interface LogObserver {
		public void updateLogs();
	}
	
	private List<LogObserver> observers = new ArrayList<LogObserver>();

	private static Logger instance = null;
	private String logs;
	
	private boolean bypassLogs = false;
	
	public static void subscribe(LogObserver observer) {
		Logger.getInstance().observers.add(observer);
	}

	private Logger() {
		logs = new String();
	}
	
	public static void log(String s) {
		Logger logger = Logger.getInstance();
		if (!logger.bypassLogs) System.out.println(s);
		logger.setLogs(logger.logs.concat(s + "\n"));
		logger.observers.forEach(LogObserver::updateLogs);
	}
	
	public static void err(String s) {
		Logger logger = Logger.getInstance();
		System.err.println(s);
		logger.setLogs(logger.logs.concat(s + "\n"));
		logger.observers.forEach(LogObserver::updateLogs);
	}

	public static Logger getInstance() {
		if (instance == null)
			instance = new Logger();
		return instance;
	}
	
	public static String getLogs() {
		return Logger.getInstance().logs;
	}
	
	public void setLogs(String s) {
		this.logs = s;
	}

}
