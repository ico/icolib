package fr.ico.lib.optimization;

import java.util.List;
import java.util.stream.Collectors;

import de.ovgu.featureide.fm.core.base.IFeature;
import de.ovgu.featureide.fm.core.configuration.Configuration;
import fr.ico.lib.optimization.storage.ConfigPerformanceItem;
import fr.ico.lib.optimization.storage.Performance;

/**
 * 
 * @author Edouard GUEGAIN
 *
 */
public class FirstLevelOptimization extends Optimization {

	public FirstLevelOptimization(ConfigImprovement configImprovement, Performance performance) {
		super(configImprovement, performance);
	}

	@Override
	public List<ConfigPerformanceItem> generateConfigPerformanceItems(Configuration config) {
		return config.getSelectedFeatures().stream()
		.filter(f -> f != null)
		.map(IFeature::getName)
		.filter(s -> this.getPerformance().ofFeature(s) != null)
		.map(s -> new ConfigPerformanceItem(this.getPerformance().ofFeature(s), s))
		.collect(Collectors.toList());
	}
	
	@Override
	public String toString() {
		return "Features";
	}
}
